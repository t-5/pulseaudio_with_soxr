#!/bin/bash

sudo apt source pulseaudio && \
sudo apt install build-essential libsoxr-dev libsoxr0 && \
sudo apt build-dep pulseaudio && \
sudo chmod 755 pulseaudio-??.? && \
cd pulseaudio-??.? && \
sudo rm -f config.log && \
sudo dpkg-buildpackage -us -uc
